package Serveur;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Matrice {
	public static final int min = 1;//min à afficher dans tab
	public static final int max = 4;//max à afficher dans tab
	public static final int longueur=9;//longueur matrice
	public static Random rand = new Random();
	public static int matrice[][] = new int[9][9];
        public static ArrayList<Pair> listcase;
        public static int parcouru[][] = new int[9][9];
        public static int score;
	
	public static int[][] generate(){
		for (int i=0; i<longueur; i++){
			for (int j=0; j<longueur; j++){
				matrice[i][j]=(rand.nextInt((max-min)+1)+min);
			}
		}
		return matrice;
	}
	
	public static void modifier(){
		for (int j=0; j<listcase.size(); j++){
			Pair pair = listcase.get(j);
			int x=pair.getX();
			int y=pair.getY();
			matrice[x][y]=(rand.nextInt((max-min)+1)+min);
		}
	}
        public static void descendre(){
            for (int y=0;y<9;y++){
                for (int x=8;x>=0;x--){
                    if (parcouru[x][y]==1){
                        int i=(x-1);
                        boolean finish=false;
                        while(i>=(-1)&&(finish==false)){
                            if (i==-1){
                                Pair caze = new Pair(x,y);
                                listcase.add(caze);
                            }
                            else if (parcouru[i][y]==0){
                                parcouru[i][y]=1;
                                matrice[x][y]=matrice[i][y];
                                parcouru[x][y]=0;
                                finish=true;
                            }
                            i--;
                        }
                    }
                }
            }
            
        }
                
        
	public static int[][] selectionner(int x, int y){
            score=0;
            listcase = new ArrayList<Pair>();
            resetParcouru();
            algo(x,y);
            descendre();
            modifier();
            return matrice;
        }
        public static int getscore(){
            return score;
        }
        public static void resetParcouru(){
            for (int i=0; i<9; i++){
			for (int j=0; j<9; j++){
				parcouru[i][j]=0;
			}
		}        
        }
        
	public static void algo(int x, int y){
                score++;
                parcouru[x][y]=1;
                if (x>0){
                    if ((matrice[x][y]==matrice[(x-1)][y])&&(parcouru[(x-1)][y]==0)){
                        algo((x-1),y);
                    }
                }
                if (x<(longueur-1)){
                    if ((matrice[x][y]==matrice[(x+1)][y])&&(parcouru[(x+1)][y]==0)){
                        algo((x+1),y);
                    }
                }
                if (y>0){
                    if ((matrice[x][y]==matrice[x][(y-1)])&&(parcouru[x][(y-1)]==0)){
                        algo(x,(y-1));
                    }
                }
                if (y<(longueur-1)){
                    if ((matrice[x][y]==matrice[x][(y+1)])&&(parcouru[x][(y+1)]==0)){
                        algo(x,(y+1));
                    }
                }
	}

}