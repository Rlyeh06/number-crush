package Serveur;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    private static ServerSocket serverSocket;
    private static Socket clientSocket;
    private static InputStreamReader inputStreamReader;
    private static BufferedReader bufferedReader;
    private static String message;
    protected final static int port = 2222; //Le port ouver du serveur est 2222
    
    static boolean first;
    static StringBuffer process;
    static String TimeStamp;
    private static final int maxClientsCount = 5;
    private static final ClientThread[] threads = new ClientThread[maxClientsCount];
    private Vue t;
    
    private static int nbJoueur(){
        int temp = 0;
        for (int i = 0; i < maxClientsCount; i++) {
            if (threads[i] != null && threads[i].getOnline()) {
                temp++;
            }
        }
        return temp;
    }
public static void main(String[] args) {
    Vue t = new Vue();
    t.setVisible(true);
    
    try {
        serverSocket = new ServerSocket(port);  
        t.getjTextArea1().append("SingleSocketServer Initialisé" + "\n");
        t.getjTextArea1().append("Serveur lancé. A l'écoute du port 2222" + "\n");
        
        int character;
        
        } catch (IOException e) {
            t.getjTextArea1().append("Impossible de se brancher sur le port: 2222" + "\n");
            
        }
         
        
        
        while (true) {
            try {
                clientSocket = serverSocket.accept(); //accept the client connection
                //////////Statu avant la connexion//////////////////////////////
                String statu ="Statu \n";
                for (int k = 0; k < maxClientsCount; k++) {
                  if (threads[k] == null) {
                      statu += "joueur " + k +  "vide \n" ;
                  }
                  else if(threads[k].getOnline()){
                      statu += "joueur " + k +  "en ligne \n" ;
                  }
                  else{
                    statu += "joueur " + k +  "hor ligne \n" ;
                  } 
                }
                t.getjTextArea1().append(statu + "\n");
                
                ////////////////////////////////////////////////////////////////
                
                
                //////////Un client se connect//////////////////////////////////
                /// Il faut le placer le client a la bonne position  ///////////
                int i = 0;
                for (i = 0; i < maxClientsCount; i++) {
                   
                    
                  
                  if (threads[i] == null || !threads[i].getOnline()) {
                      if(threads[i] != null && !threads[i].getOnline()){
                          threads[i] = new ClientThread(clientSocket, threads);
                          
                      }else{
                          threads[i] = new ClientThread(clientSocket, threads);
                          threads[i].nbJoueur = i+1;
                      }
                    threads[i].setNumeroJoueur(i+1);
                    threads[i].start();
                    
                    //threads[i].setNumeroJoueur(quelJoueur()); 
                    break;
                  }
                }
                ////////////////////////////////////////////////////////////////
                
                //////////Statu apres la connexion//////////////////////////////
                statu ="Statu \n";
               for (int k = 0; k < maxClientsCount; k++) {
                  if (threads[k] == null) {
                      statu += "joueur " + k  + "vide \n" ;
                  }
                  else if(threads[k].getOnline()){
                      statu += "joueur " + k  + "en ligne \n" ;
                  }
                  else{
                    statu += "joueur " + k + "hor ligne \n" ;
                  } 
                }
                t.getjTextArea1().append(statu + "\n");
                
                 ///////////////////////////////////////////////////////////////
                
                
                ////////////////Serveur Full////////////////////////////////////
                if (i == maxClientsCount) {
                    PrintStream os = new PrintStream(clientSocket.getOutputStream());
                    os.println("MESSAGEServer too busy. Try later.");
                    os.close();
                    clientSocket.close();
                  }
                ////////////////////////////////////////////////////////////////    
            } catch (IOException ex) {
                t.getjTextArea1().append(ex + "\n");
            }
        }
        
    }
}

