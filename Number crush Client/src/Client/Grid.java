package Client;

import java.awt.GridLayout;
import javax.swing.JPanel;

/**
 *
 * @author robin
 */

public class Grid {
    public static final int longueur=9;
    public static int tab[][] = new int[9][9];
    public static JPanel panel;
    private static GridLayout grid;

    public Grid(JPanel panel) {
        this.panel = panel;
        grid = new GridLayout(longueur, longueur);
        afficher();
    }
    /**
     * Met a jour ma matrice de bouttons
     * prend une nouvelle matrice de int de valeur 1 a 4
     * @param newtab Matrice
     */
    public void actualier(int[][] newtab){
        for(int r = 0; r < longueur; r++){
                for(int c = 0; c < longueur; c++){
                       getGridButton(r,c).setButton(""+ newtab[r][c]);
                }
        }
        
    }
    /**
     * Recuper sur la gruille le boutton ChangingButton (x,y)
     * 
     * @param x 
     * @param y
     * @return 
     */
    private ChangingButton getGridButton(int x, int y) {
        int index = x * longueur + y;
        return (ChangingButton)panel.getComponent(index);
    }
    /**
     * mais en place la grille
     */
    public static void afficher(){
        panel.setLayout(grid);
        for(int r = 0; r < longueur; r++){
                for(int c = 0; c < longueur; c++){
                        ChangingButton button= new ChangingButton(r, c, tab[r][c]);
                        panel.add(button);
                }
        }
	} 
    
}
