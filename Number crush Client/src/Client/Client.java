package Client;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;



public class Client {
    public static PrintWriter pw = null;
    public static boolean run;
    
    /**
     * Cet fonction permet d'envoyer un message au serveur
     * fonctionne uniquemment si le client est conneté
     * 
     * @param message  String
     */
    public static void send(String message){
                   pw.println(message); 
                }
    
    /**
     * Cet fonction envoyer un message de déconection
     * qui déconnecte le client du serveur
     * 
     */
    public static void logout(){
           pw.println("DECONNEXTION"); 
            }
    
	public static void main(String[] args) throws Exception {
		String adresseIP; // adresse de connexion
		String login; // Nom du joueur
		int port = 2222; // port du serveur
                Login log = new Login(); // on crée une fennetre qui nous demmande de choisir un nom
                log.setVisible(true);
                
                while(log.isVisible()){
                    System.out.println("on attend que le client choisi son nom");
                    /// on attend
                }
		login = log.login; // le login demandé part la fennetre precendent est eregisté
                adresseIP = log.adresse; // le login demandé part la fennetre precendent est eregisté
                
                ///////////////////////////////////////
                // on lance l'interface             ///
                
                Interface t = new Interface();
                t.setVisible(true);
                t.getjLabel1().setText(login);
                //////////////////////////////////////
                
                
		///////////Protocol de connection///////////////////
		InetAddress address = InetAddress.getByName(adresseIP);
		// Connexion TCP au serveur
                Socket socket = null;
                try{
		socket = new Socket(address, port);
                t.getjTextArea1().append("Connecté" + "\n");
		System.out.println("SOCKET = " + socket);
                } catch(Exception e){
                    System.out.println("Connexion impossible");
                    System.exit(0);
                }

		// Pour lire ce que serveur transmet
		BufferedReader br = 
				new BufferedReader(
						new InputStreamReader(socket.getInputStream())
						);
                // Pour lire au clavier
		BufferedReader clavier = new BufferedReader(
                        new InputStreamReader(System.in));
		// Pour transmettre au serveur
		pw = new PrintWriter(new BufferedWriter(
                        new OutputStreamWriter(socket.getOutputStream())),
                        true);
                ///////////////////////////////////////////////
               
		pw.println("@"+login); // transmission du login
                String reponse; //Buffer pour la reception d'une reponse ou message serveur
		reponse = br.readLine();

		System.out.println(reponse);
                Client.send("NEW");
		
                run = true;
		while(run) {//Boucle de dialogue
                    reponse = br.readLine();
                    
                    //////////////////Fin de partie/////////////////////////////
                    //                                                       ///
                    if(reponse.startsWith("FIN")){
                        reponse = reponse.substring(3);
                        String[] rep = reponse.split(";");
                        Finish f = new Finish(rep[0],rep[1]);
                    }
                    ///                                                      ///
                    ////////////////////////////////////////////////////////////
                    
                    //////////////////Message de deconection////////////////////
                    //                                                       ///
                    if(reponse.startsWith("DECO")){
                        break;
                    }
                    ///                                                      ///
                    ////////////////////////////////////////////////////////////
                    
                    
                    //////////////////Reseption des messages////////////////////
                    //                                                       ///
                    if (reponse.startsWith("MESSAGE")){
                        reponse = reponse.substring(7);
                        t.getjTextArea1().append(reponse + "\n");
                        
                    }
                    ///                                                      ///
                    ////////////////////////////////////////////////////////////
                        
                     
                    /////////////////Reseption des statues//////////////////////
                    ///                                                      ///
                    if (reponse.startsWith("STATU")){
                        reponse = reponse.substring(5);
                        String[] temp = reponse.split("@");
                        t.setinfo(temp);
                        
                        //t.getjTextArea1().append(reponse + "\n");
                        
                    }
                    ///                                                      ///
                    ////////////////////////////////////////////////////////////
                    
                    
                    /////////////////Demmand de vote////////////////////////////
                    ///                                                      ///
                    if (reponse.startsWith("VOTE")){
                        t.demmandeNouvelPartie();
                    }
                    ///                                                      ///
                    ////////////////////////////////////////////////////////////
                    
                    
                    /////////////////Nouvel Grille//////////////////////////////
                    /// Le serveur nous envoi une nouvelle grille            ///
                    if (reponse.startsWith("G")){
                            reponse = reponse.substring(1, reponse.length());
                            System.out.println(reponse);
                            int[][] nouvelGrille = new int[9][9];
                            int j = -1;
                            for(int i = 0; i < reponse.length();i++){
                                if(i%9 == 0){
                                   j++; 
                                }
                                   nouvelGrille[j][i%9]= Integer.parseInt("" 
                                           + reponse.charAt(i));    
                            }
                            t.newGrid(nouvelGrille);
                        }
                    ///                                                      ///
                    ////////////////////////////////////////////////////////////
			if(!run) {
				break;
			}
			
                        
                       
			System.out.println(reponse);
		}
		pw.close();
		br.close();
		socket.close();       
	}
}
